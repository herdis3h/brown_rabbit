const randomSlide = Math.floor(
  Math.random() * $("#carousel .carousel-inner .carousel-item").length
);

$(document).ready(function() {
  const indicators = $(".carousel-indicators li");
  const items = $(".carousel-item");

  $.each(indicators, function(index, indicator) {
    if (randomSlide == indicator.getAttribute("data-slide-to")) {
      $(this).addClass("active");
    }
  });
  $.each(items, function(index, item) {
    if (randomSlide == index) {
      $(this).addClass("active");
    }
  });

  $("#searchInput").on("keyup", function() {
    var value = $(this)
      .val()
      .toLowerCase();
    $(".post h2").filter(function() {
      $(this)
        .parent()
        .parent()
        .toggle(
          $(this)
            .text()
            .toLowerCase()
            .indexOf(value) > -1
        );
    });
  });

  $(".content-text").each(function() {
    var content = $(this).html();

    var t = $(this).text();
    if (t.length < 120) return;

    $(this).html(
      t.slice(0, 120) +
        "<span class='dots'>... </span>" +
        '<span class="more">' +
        t.slice(120, t.length) +
        "</span>"
    );
  });

  $(".nav-toggle").click(function(event) {
    if ($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this)
        .prev()
        .find(".more")
        .removeClass("show-more");
      $(this)
        .prev()
        .find(".dots")
        .css("display", "contents");
    } else {
      $(this).addClass("less");
      $(this)
        .prev()
        .find(".more")
        .addClass("show-more");

      $(this)
        .prev()
        .find(".dots")
        .css("display", "none");
    }
  });

  var numberOfItems = $("#content .post").length;
  var limitPerPage = 4;
  $("#content .post:gt(" + (limitPerPage - 1) + ")").hide();
  var totalPages = Math.ceil(numberOfItems / limitPerPage);

  $(".pagination").append(
    "<li class=' page-item '><a class='page-link' href='javascript:void(0)'>Page " +
      1 +
      " of " +
      totalPages +
      " </a></li>"
  );

  $(".pagination").append(
    "<li class=' page-item current-page active'><a class='page-link' href='javascript:void(0)'>" +
      1 +
      "</a></li>"
  );
  for (var i = 2; i <= totalPages; i++) {
    $(".pagination").append(
      "<li class=' page-item  current-page'><a class='page-link' href='javascript:void(0)'>" +
        i +
        "</a></li>"
    );
  }
  $(".pagination").append(
    "<li class='page-item'  id='next-page'><a href='javascript:void(0)' class='page-link' aria-label=Next><span aria-hidden=true>&raquo;</span></a></li>"
  );

  $(".pagination li.current-page").on("click", function() {
    if ($(this).hasClass("active")) {
      return false;
    } else {
      var currentPage = $(this).index();
      console.log("currentPage", currentPage);

      $(".pagination li").removeClass("active");
      $(this).addClass("active");
      $("#content .post").hide();

      var grandTotal = limitPerPage * currentPage;

      for (var i = grandTotal - limitPerPage; i < grandTotal; i++) {
        $("#content .post:eq(" + i + ")").show();
      }
    }
  });

  $("#next-page").on("click", function() {
    var currentPage = $(".pagination li.active").index();

    if (currentPage === totalPages) {
      return false;
    } else {
      currentPage++;
      $(".pagination li").removeClass("active");
      $("#content .post").hide();
      var grandTotal = limitPerPage * currentPage;

      for (var i = grandTotal - limitPerPage; i < grandTotal; i++) {
        $("#content .post:eq(" + i + ")").show();
      }

      $(".pagination li.current-page:eq(" + (currentPage - 1) + ")").addClass(
        "active"
      );
    }
  });
});
